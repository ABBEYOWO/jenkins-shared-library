#!/usr/bin/env groovy

def call() {
    echo 'building a docker image ...'
    withCredentials([usernamePassword(credentialsId: 'Docker-Hub-Credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
    sh 'docker build -t newera24/demo-app:jma-2.1 .'
    sh 'echo $PASS | docker login -u $USER --password-stdin'
    sh 'docker push newera24/demo-app:jma-2.1'
    }
}
